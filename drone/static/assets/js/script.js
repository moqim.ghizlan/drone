function changeImagePos() {
    const cards = document.querySelectorAll('.cards-images');
    cards.forEach(card => {
        card.addEventListener('mouseover', () => {
            card.childNodes[1].classList.add('hidden-img');
            card.childNodes[1].classList.remove('showen-img');
            card.childNodes[1].style.transition = "all 0.5s ease-in-out";
            card.childNodes[3].classList.add('showen-img');
            card.childNodes[3].classList.remove('hidden-img');
            card.childNodes[3].style.transition = "all 0.5s ease-in-out";
        });
        card.addEventListener('mouseout', () => {
            card.childNodes[1].classList.add('showen-img');
            card.childNodes[1].classList.remove('hidden-img');
            card.childNodes[1].style.transition = "all 0.5s ease-in-out";
            card.childNodes[3].classList.add('hidden-img');
            card.childNodes[3].classList.remove('showen-img');
            card.childNodes[3].style.transition = "all 0.5s ease-in-out";
        });
    });
}

function changeImageFocus() {
    const mainImage = document.querySelector('#main-image');
    const Allimages = document.querySelectorAll('.all-images');
    Allimages.forEach(image => {
        image.addEventListener('mouseover', () => {
            mainImage.src = image.src;
        });
    });
}


function setImageModale() {
    const openModalButton = document.getElementById("main-image");
    openModalButton.addEventListener("click", function () {
        const modal = document.getElementById("image-modal");
        const closeButton = document.getElementsByClassName("close")[0];
        const modalImage = document.getElementById("modal-image");
        const sourceImage = document.getElementById("main-image").src;
        modalImage.src = sourceImage
        modal.style.display = "block";
        closeButton.addEventListener("click", function () {
            modal.style.display = "none";
        });
    });
}

function closeImageOnDoubleClicks() {
    var modal = document.getElementById("image-modal");
    modal.style.display = "none";
}

function drowStars() {
    const starsHolders = document.querySelectorAll(".stars-holder");
    starsHolders.forEach(starHolder => {
        const starsHolderValue = starHolder.children[0].value;
        const nb = parseInt(starsHolderValue)
        for (let i = 1; i < nb + 1; i++) {
            starHolder.children[i].classList.add("star-checked");
        }
    });
}


function createStar() {
    const starsHolders = document.querySelectorAll(".stars-holder");
    starsHolders.forEach(starHolder => {
        const starsHolderValue = starHolder.getAttribute('data-stars-holder-value');
        const yallowStars = parseInt(starsHolderValue)
        for (let i = 0; i < 5; i++) {
            const star = document.createElement("span");
            star.classList.add("fa");
            star.classList.add("fa-star");
            starHolder.appendChild(star);
        }
        for (let i = 0; i < yallowStars; i++) {
            starHolder.children[i].classList.add("star-checked");
        }

    });

}
