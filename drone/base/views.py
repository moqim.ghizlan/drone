from multiprocessing import context
import re
from turtle import pos
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate
from django.contrib.auth import login
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.db.models import Q
from django.contrib.auth.hashers import check_password, make_password
from base.formes.login import NewUserForm
from .models import *



def page_404(request):
    return render(request, '404.html', {})

def index(request):
    drones = Drone.objects.all()
    for drone in drones:
        drone.images = []
        drone.images += Drone_Images.objects.filter(drone = drone).all()
        drone.description = drone.description[0:80]
    return render(request, 'index.html', {"drones" : drones})

def drone(request, id):
    drone = Drone.objects.filter(id=id).first()
    drone.images = []
    drone.images += Drone_Images.objects.filter(drone = drone).all()
    return render(request, 'drone.html', {"drone" : drone})


def registe(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            # login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("index")
        messages.error(request, "Unsuccessful registration. Invalid information.")
        return render(request=request, template_name="register.html", context={"register_form": form, 'error__':f'{form.errors}'})
    form = NewUserForm()
    return render(request=request, template_name="register.html", context={"register_form": form})

