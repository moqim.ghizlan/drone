from django.db import models
from datetime import datetime, timedelta, date
from django.utils.html import format_html
from django.template.defaultfilters import date as django_date
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    first_name = models.CharField(max_length=150, null=False)
    last_name = models.CharField(max_length=150, null=False)
    email = models.EmailField(max_length=254, null=False, unique=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ['first_name', 'last_name', 'username']

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

class Company(models.Model):
    name = models.CharField(max_length=150, null=False)
    country = models.CharField(max_length=150, null=False)
    def __str__(self):
        return f'{self.name}'

class Resolution(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False)
    height = models.IntegerField(blank=False, null=False)
    wigth = models.IntegerField(blank=False, null=False)
    def __str__(self):
        return f'{self.name}  {self.height}x{self.wigth}'

class Drone(models.Model):
    title = models.CharField(max_length=150, blank=False, null=False)
    description = models.CharField(max_length=1024, blank=False, null=False)
    rating = models.FloatField(blank=False, null=False)
    price = models.FloatField(blank=False, null=False)
    weight = models.FloatField(blank=False, null=False)
    speed = models.FloatField(blank=False, null=False)
    bat_cap_h = models.FloatField(blank=False, null=False)
    bat_cap_w = models.FloatField(blank=False, null=False)
    flight_time = models.FloatField(blank=False, null=False)
    flight_range = models.FloatField(blank=False, null=False)
    transmission_range = models.FloatField(blank=False, null=False)
    dimensions_width = models.FloatField(blank=False, null=False)
    dimensions_height = models.FloatField(blank=False, null=False)
    dimensions_deepth = models.FloatField(blank=False, null=False)
    resolution = models.ForeignKey(Resolution, on_delete=models.CASCADE, related_name="resolution")
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="drone")

    def __str__(self):
        return f'{self.title}'

class Drone_Images(models.Model):
    drone = models.ForeignKey(Drone, default=None, on_delete=models.CASCADE)
    url = models.FileField(upload_to = 'drone_images/')

    def __str__(self):
        return self.drone.title


class Favorites_Drone(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    drones = models.ManyToManyField(Drone)
    def __str__(self):
        return f'{self.user} -> Favorites'


class Basket(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="Basket_user")
    drones = models.ManyToManyField(Drone)
    def __str__(self):
        return f'{self.user}'


