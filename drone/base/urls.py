from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('', views.index, name='index'),
    path('droneId=<int:id>', views.drone, name='drone'),
    path('404', views.page_404, name='page_404'),
    path('registe', views.registe, name='registe'),
]+ static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)