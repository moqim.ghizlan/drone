from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from ..models import *
from django.utils.translation import gettext_lazy as _
User = get_user_model()


class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email", "password1", "password2")
        labels = {
            'first_name': _('Prénom'),
            'last_name': _('Nom'),
            'username': _("Nom d'utilisateur"),
            'email': _('Adresse mail'),
            'password1': _('Mot de passe'),
            'password2': _('Confirmation du mot de passe'),
            }
    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user
