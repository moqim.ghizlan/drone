from django.contrib import admin
from base.models import *

admin.site.register(User)
admin.site.register(Company)
# admin.site.register(Drone)
admin.site.register(Favorites_Drone)
admin.site.register(Basket)
admin.site.register(Resolution)


class DroneImageAdmin(admin.StackedInline):
    model = Drone_Images

@admin.register(Drone)
class PostAdmin(admin.ModelAdmin):
    inlines = [DroneImageAdmin]

    class Meta:
       model = Drone

@admin.register(Drone_Images)
class DroneImageAdmin(admin.ModelAdmin):
    pass